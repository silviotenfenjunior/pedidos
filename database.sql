-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           10.1.34-MariaDB-0ubuntu0.18.04.1 - Ubuntu 18.04
-- OS do Servidor:               debian-linux-gnu
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela mercos_system.ox20q_customers
CREATE TABLE IF NOT EXISTS `ox20q_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mercos_system.ox20q_customers: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `ox20q_customers` DISABLE KEYS */;
INSERT INTO `ox20q_customers` (`id`, `name`) VALUES
	(1, 'Darth Vader'),
	(2, 'Obi-Wan Kenobi'),
	(3, 'Luke Skywalker'),
	(4, 'Imperador Palpatine'),
	(5, 'Hann Solo');
/*!40000 ALTER TABLE `ox20q_customers` ENABLE KEYS */;

-- Copiando estrutura para tabela mercos_system.ox20q_orders
CREATE TABLE IF NOT EXISTS `ox20q_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ox20q_orders_ox20q_customers1_idx` (`customer_id`),
  CONSTRAINT `fk_ox20q_orders_ox20q_customers1` FOREIGN KEY (`customer_id`) REFERENCES `ox20q_customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mercos_system.ox20q_orders: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ox20q_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `ox20q_orders` ENABLE KEYS */;

-- Copiando estrutura para tabela mercos_system.ox20q_orders_items
CREATE TABLE IF NOT EXISTS `ox20q_orders_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ox20q_orders_items_ox20q_orders1_idx` (`order_id`),
  KEY `fk_ox20q_orders_items_ox20q_products1_idx` (`product_id`),
  CONSTRAINT `fk_ox20q_orders_items_ox20q_orders1` FOREIGN KEY (`order_id`) REFERENCES `ox20q_orders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ox20q_orders_items_ox20q_products1` FOREIGN KEY (`product_id`) REFERENCES `ox20q_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mercos_system.ox20q_orders_items: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ox20q_orders_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ox20q_orders_items` ENABLE KEYS */;

-- Copiando estrutura para tabela mercos_system.ox20q_products
CREATE TABLE IF NOT EXISTS `ox20q_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `multiple` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mercos_system.ox20q_products: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `ox20q_products` DISABLE KEYS */;
INSERT INTO `ox20q_products` (`id`, `name`, `unit_price`, `multiple`) VALUES
	(1, 'Millenium Falcon', 550000.00, NULL),
	(2, 'X-Wing', 60000.00, 2),
	(3, 'Super Star Destroyer', 4570000.00, NULL),
	(4, 'TIE Fighter', 75000.00, 2),
	(5, 'Lightsaber', 6000.00, 5),
	(6, 'DLT-19 Heavy Blaster Rifle', 5800.00, NULL),
	(7, 'DLT-44 Heavy Blaster Pistol', 1500.00, 10);
/*!40000 ALTER TABLE `ox20q_products` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
