<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4624b3300cf48aefb8f5eb9752f1488d
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PhpParser\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PhpParser\\' => 
        array (
            0 => __DIR__ . '/..' . '/nikic/php-parser/lib/PhpParser',
        ),
    );

    public static $prefixesPsr0 = array (
        'o' => 
        array (
            'org\\bovigo\\vfs' => 
            array (
                0 => __DIR__ . '/..' . '/mikey179/vfsStream/src/main/php',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4624b3300cf48aefb8f5eb9752f1488d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4624b3300cf48aefb8f5eb9752f1488d::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit4624b3300cf48aefb8f5eb9752f1488d::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
