<?php
// System Settings File

$CI =& get_instance();
$CI->load->helper('url');

$config['system_name'] = 'Pedidos';
$config['system_logo'] = base_url('assets/imgs/logo.png');
$config['email_logo'] = $config['system_logo'];
$config['favicon'] = base_url('assets/imgs/favicon.png');
$config['favicon2'] = base_url('assets/imgs/apple-touch-icon.png');
$config['company_name'] = 'Silvio Tenfen Junior';
$config['default_title'] = $config['system_name'].' - Painel de Administração';
?>
