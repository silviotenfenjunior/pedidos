<?php
// Configuracoes padroes para upload de arquivos
$config['upload_path'] = 'tmp/uploads/';
$config['allowed_types'] = 'png';
$config['max_size'] = 5120;
$config['overwrite'] = true;
$config['file_name'] = '0.png';
?>