<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CONTROLLER Products
 *
 * Products Manager Controller
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package products
 */

class Products extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->model('Products_model');
    }

    function get_products() {
        $term = isset($_GET['term']) ? $_GET['term'] : '';

        $json = array();
        if ($term != '') {
            $regs = $this->Products_model->getAll(0, 0, $term);

            foreach ($regs as $reg) {
                $json[] = array(
                    'id' => $reg->id,
                    'name' => $reg->name
                );
            }
        }

        echo json_encode($json);
    }

    function get_product($id = 0) {
        $json = array();
        if ($id != 0) {
            $json = $this->Products_model->getRow('id', $id);
        }

        echo json_encode($json);
    }
}

?>