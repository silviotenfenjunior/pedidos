<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CONTROLLER Orders
 *
 * Order Manager Controller
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package orders
 */

class Orders extends CI_Controller {

    private $_layout = 'general/layout';
    private $_data;
    private $_id = 0;
    
    function __construct() {
        parent::__construct();

        $this->load->helper('money');

        $this->load->library('Frontend');

        $this->load->model('Customers_model');
        $this->load->model('Orders_model');
        $this->load->model('Orders_items_model');
        $this->load->model('Products_model');
    }
    
    function index($offset = 0, $limpar = 0) {
        $this->load->library('pagination');
        $this->load->library('table');

        $search = $this->input->post('search');
        $name = $this->session->userdata('pesq_periodos_name');

        if ($search == 1) {
            $name = $this->input->post('name');
            $this->session->set_userdata('pesq_periodos_name',$name);
        }
        
        if ($limpar == 1) {
            $name = '';
            $this->session->unset_userdata('pesq_periodos_name');
        }

        $this->_data['data'] = array(
            'name' => $name
        );
        
        $per_page = 30;
        $records = $this->Orders_model->getAll($per_page,$offset,$name);
        $this->_data['nrecords'] = $this->Orders_model->getCountAll($name);

        $data = array();
        foreach ($records as $rec) {

            $edit = anchor('orders/edit/'.$rec->id, '<i class="uk-icon-pencil"></i>', 'title="Editar"');
            $delete = ' <a href="#" onclick="deleteRecord('.$rec->id.')"><i class="uk-icon-remove" title="Remover"></i></a>';
            $options = '<div class="uk-text-right">'.$edit.$delete.'</div>';

            $data[] = array(
                $rec->id,
                $rec->customer,
                $options
             );
        }
        $this->_data['records'] = $data;
        
        $config['base_url'] = base_url().'orders/index/';
        $config['total_rows'] = $this->_data['nrecords'];
        $config['per_page'] = $per_page;
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['num_links'] = 10;
        $config['uri_segment'] = 3;

        $config['cur_tag_open'] = '<li class="uk-active"><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        
        $this->pagination->initialize($config);
        $this->_data['pages'] = $this->pagination->create_links();

        $this->_data['question'] = 'Deseja realmente remover o Pedido?';
        $this->_data['link_to_remove'] = base_url('orders/delete');
        
        $this->_data['page'] = 'orders/list';
        $this->load->view($this->_layout, $this->_data);
    }

    function add() {
        $this->form_validation->set_rules('customer_id', 'Cliente', 'required|trim|xss_clean|integer');
        $this->form_validation->set_rules('items', 'Itens', '');

        $this->form_validation->set_error_delimiters('<br/><span class="uk-badge uk-badge-danger">', '</span>');
        
        $this->_data['function'] = 'add';
        $this->_data['form_name'] = 'Adicionar Pedido';
        
        $this->_data['data'] = array(
            'id' => '',
            'customer_id' => '',
            'customer_name' => ''
        );

        if ($this->form_validation->run() == FALSE) {
            // Nao mostrar todos os erros de uma vez, dando preferencia
            // para mostrar os erros individualmente por campo
            $this->_data['not_show_validation_errors'] = 1;
            $this->_data['page'] = 'orders/form';
            $this->load->view($this->_layout, $this->_data);

        } else {
            $form_data = array(
                'customer_id' => set_value('customer_id'),
            );

            if ( $this->Orders_model->insert($form_data) ) {
                $msg_adicional = '';
                if ( !$this->update_items( set_value('items'), $this->db->insert_id() ) ) {
                    $msg_adicional = 'Porém houve um erro ao adicionar os itens do pedido!';
                }
                $msg['success'] = 'O Pedido foi adicionado com sucesso! '.$msg_adicional;
            } else {
                $msg['error'] = 'Houve um erro ao adicionar o Pedido!';
            }
            
            $this->session->set_userdata('msg', $msg);
            redirect('orders');
        }
    }

    function edit($id) {
        $this->form_validation->set_rules('customer_id', 'Cliente', 'required|trim|xss_clean|integer');
        $this->form_validation->set_rules('items', 'Itens', '');

        $this->form_validation->set_error_delimiters('<br/><span class="uk-badge uk-badge-danger">', '</span>');
        
        $this->_data['function'] = 'edit';
        $this->_data['form_name'] = 'Editar Pedido';
        
        // Usado pelo checagem de duplicidade.
        $this->_id = $id;
        
        $order = $this->Orders_model->getRow('id', $id);
        $order_items = $this->Orders_items_model->getAll(0, 0, $id);

        $order_items2 = array();
        foreach ($order_items as $rec) {
            $order_items2[] = array(
                'attrs' => 'class="order_item_'.$rec->product_id.'" data-id="'.$rec->product_id.'" data-original_unit_price="'.$rec->original_unit_price.'" data-multiple="'.$rec->multiple.'"',
                'product' => $rec->name,
                'qty' => '<input type="number" name="items['.$rec->product_id.'][qty]" value="'.$rec->qty.'" class="uk-width-1-1 recalculate" data-id="'.$rec->product_id.'" />',
                'unit_price' => '<input type="text" name="items['.$rec->product_id.'][unit_price]" value="'.number_format($rec->unit_price, 2, ',', '.').'" class="uk-width-1-1 money recalculate" data-id="'.$rec->product_id.'" />',
                'subtotal' => '',
                'profitability' => '',
                'actions' => '<a href="#" onclick="deleteItem('.$rec->product_id.')" title="Remover"><i class="uk-icon-remove"></i></a>'
            );
        }

        $this->_data['data'] = array(
            'id' => $id,
            'customer_id' => $order->customer_id,
            'customer_name' => $order->name,
            'order_items' => $order_items2
        );

        if ($this->form_validation->run() == FALSE) {
            // Nao mostrar todos os erros de uma vez, dando preferencia
            // para mostrar os erros individualmente por campo
            $this->_data['not_show_validation_errors'] = 1;
            $this->_data['page'] = 'orders/form';
            $this->load->view($this->_layout, $this->_data);

        } else {
            $form_data = array(
                'customer_id' => set_value('customer_id'),
            );

            if ( $this->Orders_model->update($form_data, $id) ) {
                $msg_adicional = '';
                if ( !$this->update_items( set_value('items'), $id) ) {
                    $msg_adicional = 'Porém houve um erro ao atualizar os itens do pedido!';
                }
                $msg['success'] = 'O Pedido foi atualizado com sucesso! '.$msg_adicional;

            } else {
                $msg['error'] = 'Houve um erro ao atualizar o Pedido!';
            }
            
            $this->session->set_userdata('msg',$msg);
            redirect('orders');
        }
    }

    function get_customers() {
        $term = isset($_GET['term']) ? $_GET['term'] : '';

        $json = array();
        if ($term != '') {
            $regs = $this->Customers_model->getAll(0, 0, $term);

            foreach ($regs as $reg) {
                $json[] = array(
                    'id' => $reg->id,
                    'name' => $reg->name
                );
            }
        }

        echo json_encode($json);
    }

    function update_items($form_data, $order_id) {
        // Get product_id from list of bd items
        $order_items = $this->Orders_items_model->getAll(0, 0, $order_id);
        $order_items_db = array();
        foreach ($order_items as $rec1) {
            $order_items_db[] = $rec1->product_id;
        }

        // get product_id from list created / changed
        $form_data2 = array();
        foreach ($form_data as $product_id => $rec) {
            $form_data2[] = $product_id;
        }

        // compare arrays and check which ones need to be added in bd
        $to_add = array_diff($form_data2, $order_items_db);
        foreach ($to_add as $product_id) {
            $form_data3 = array(
                'order_id' => $order_id,
                'product_id' => $product_id,
                'qty' => $form_data[$product_id]['qty'],
                'unit_price' => brl2decimal( $form_data[$product_id]['unit_price'] )
            );

            if ( !$this->Orders_items_model->insert($form_data3) ) {
                return false;
            }
        }

        // compare arrays and check which ones need to be updated in bd
        $to_update = array_intersect($order_items_db, $form_data2);
        foreach ($to_update as $product_id) {
            $form_data3 = array(
                'qty' => $form_data[$product_id]['qty'],
                'unit_price' => brl2decimal( $form_data[$product_id]['unit_price'] )
            );

            if ( !$this->Orders_items_model->update($form_data3, 'product_id', $product_id, $order_id) ) {
                return false;
            }
        }
        
        // compare arrays and check which ones need to be deleted in bd
        $to_delete = array_diff($order_items_db, $form_data2);
        foreach ($to_delete as $product_id) {
            if ( !$this->Orders_items_model->delete('product_id', $product_id, $order_id) ) {
                return false;
            }
        }

        return true;
    }
    
    function delete($id) {

        if ( $this->Orders_items_model->delete('order_id', $id) ) {
            if ( $this->Orders_model->delete('id', $id) ) {
                $msg['success'] = 'O Pedido foi removido com sucesso!';
            } else {
                $msg['error'] = 'Houve um erro ao remover o Pedido!';
            }

        } else {
            $msg['error'] = 'Houve um erro ao remover os itens do Pedido!';
        }

        $this->session->set_userdata('msg', $msg);
        redirect('orders');
    }

}

?>