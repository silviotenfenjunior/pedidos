<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MODEL Products_model
 *
 * Customer Table Model
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package products
 */

class Products_model extends CI_Model {

    private $_tab_products = 'products';

    function getAll($per_page, $offset, $name = '') {
        $this->db->order_by('id');

        if ($name != '') {
            $this->db->like('name', $name);
        }

        $this->db->select('*');
        $this->db->from($this->_tab_products);
        $this->db->limit($per_page, $offset);

        $query = $this->db->get();
        return $query->result();
    }

    function getCountAll($name = '') {
        if ($name != '') {
            $this->db->like('name', $name);
        }

        $this->db->from($this->_tab_products);

        return $this->db->count_all_results();
    }

    function getRow($field, $value, $ignore_id = '') {
        $this->db->where($field, $value);

        if ($ignore_id != '') {
            $this->db->where('ignore_id !=', $ignore_value);
        }

        $query = $this->db->get($this->_tab_products);
        return $query->row(0);
    }

    function insert($data) {
        $this->db->insert($this->_tab_products, $data);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->_tab_products, $data);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->_tab_products);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

?>