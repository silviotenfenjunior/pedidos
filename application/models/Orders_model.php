<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MODEL Orders_model
 *
 * Order Table Model
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package orders
 */

class Orders_model extends CI_Model {

    private $_tab_orders = 'orders';
    private $_tab_customers = 'customers';

    function getAll($per_page, $offset, $name = '') {
        $this->db->order_by('id');

        if ($name != '') {
            $this->db->like('name', $name);
        }

        $this->db->select(
            $this->_tab_orders.'.*,'.
            $this->_tab_customers.'.name as customer'
        );
        $this->db->from($this->_tab_orders);
        $this->db->join($this->_tab_customers, $this->_tab_orders.'.customer_id = '.$this->_tab_customers.'.id', 'left');
        $this->db->limit($per_page, $offset);

        $query = $this->db->get();
        return $query->result();
    }

    function getCountAll($name = '') {
        if ($name != '') {
            $this->db->like('name', $name);
        }

        $this->db->from($this->_tab_orders);
        $this->db->join($this->_tab_customers, $this->_tab_orders.'.customer_id = '.$this->_tab_customers.'.id', 'left');

        return $this->db->count_all_results();
    }

    function getRow($field, $value, $ignore_id = '') {
        $this->db->where($this->_tab_orders.'.'.$field, $value);

        if ($ignore_id != '') {
            $this->db->where($this->_tab_orders.'.id !=', $ignore_id);
        }

        $this->db->from($this->_tab_orders);
        $this->db->join($this->_tab_customers, $this->_tab_orders.'.customer_id = '.$this->_tab_customers.'.id', 'left');

        $query = $this->db->get();
        return $query->row(0);
    }

    function insert($data) {
        $this->db->insert($this->_tab_orders, $data);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->_tab_orders, $data);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete($field, $value) {
        $this->db->where($field, $value);
        $this->db->delete($this->_tab_orders);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

?>