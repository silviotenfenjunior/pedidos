<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MODEL Customers_model
 *
 * Customer Table Model
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package customers
 */

class Customers_model extends CI_Model {

    private $_tab_customers = 'customers';

    function getAll($per_page, $offset, $name = '') {
        $this->db->order_by('id');

        if ($name != '') {
            $this->db->like('name', $name);
        }

        $this->db->select('*');
        $this->db->from($this->_tab_customers);
        $this->db->limit($per_page, $offset);

        $query = $this->db->get();
        return $query->result();
    }

    function getCountAll($name = '') {
        if ($name != '') {
            $this->db->like('name', $name);
        }

        $this->db->from($this->_tab_customers);

        return $this->db->count_all_results();
    }

    function getRow($field, $value, $ignore_id = '') {
        $this->db->where($field, $value);

        if ($ignore_id != '') {
            $this->db->where('id !=', $ignore_id);
        }

        $query = $this->db->get($this->_tab_orders);
        return $query->row(0);
    }

    function insert($data) {
        $this->db->insert($this->_tab_customers, $data);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function update($data, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->_tab_customers, $data);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->_tab_customers);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

?>