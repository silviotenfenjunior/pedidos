<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MODEL Orders_items_model
 *
 * Order Table Model
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package orders
 */

class Orders_items_model extends CI_Model {

    private $_tab_orders_items = 'orders_items';
    private $_tab_products = 'products';

    function getAll($per_page, $offset, $order_id = '') {
        $this->db->order_by($this->_tab_orders_items.'.id');

        if ($order_id != '') {
            $this->db->where('order_id', $order_id);
        }

        $this->db->select(
            $this->_tab_orders_items.'.*,'.
            $this->_tab_products.'.name,'.
            $this->_tab_products.'.unit_price as original_unit_price,'.
            $this->_tab_products.'.multiple'
        );
        $this->db->from($this->_tab_orders_items);
        $this->db->join($this->_tab_products, $this->_tab_orders_items.'.product_id = '.$this->_tab_products.'.id', 'left');
        $this->db->limit($per_page, $offset);

        $query = $this->db->get();
        return $query->result();
    }

    function getCountAll($order_id = '') {
        if ($order_id != '') {
            $this->db->where('order_id', $order_id);
        }

        $this->db->from($this->_tab_orders_items);
        $this->db->join($this->_tab_products, $this->_tab_orders_items.'.product_id = '.$this->_tab_products.'.id', 'left');

        return $this->db->count_all_results();
    }

    function getRow($field, $value, $ignore_id = '') {
        $this->db->where($this->_tab_orders_items.'.'.$field, $value);

        if ($ignore_id != '') {
            $this->db->where($this->_tab_orders_items.'.id !=', $ignore_id);
        }

        $this->db->from($this->_tab_orders_items);
        $this->db->join($this->_tab_orders, $this->_tab_orders_items.'.customer_id = '.$this->_tab_orders.'.id', 'left');

        $query = $this->db->get();
        return $query->row(0);
    }

    function insert($data) {
        $this->db->insert($this->_tab_orders_items, $data);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function update($data, $field, $value, $order_id = 0) {
        $this->db->where($field, $value);
        $this->db->update($this->_tab_orders_items, $data);

        if ($order_id != 0) {
            $this->db->where('order_id', $order_id);
        }

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }

    function delete($field, $value, $order_id = 0) {
        $this->db->where($field, $value);

        if ($order_id != 0) {
            $this->db->where('order_id', $order_id);
        }

        $this->db->delete($this->_tab_orders_items);

        $res = $this->db->error();
        if ($res['code'] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

?>