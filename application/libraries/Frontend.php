<?php

/**
 * LIBRARY Frontend
 *
 * Library to Handle Content Regarding All Site Pages (such as Head and Footer)
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package frontend
 */

class Frontend {
	private $_data;

	public function __construct() {
		$this->_data['js_code'] = '';
	}

	public function setJsCode($code) {
		$this->_data['js_code'] .= $code."\n";
	}

	public function getJsCode() {
		return $this->_data['js_code'];
	}
}

?>