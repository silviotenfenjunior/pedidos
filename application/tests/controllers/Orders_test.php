<?php
/**
 * Orders Test 
 *
 * @author     Silvio Tenfen Junior <silvio@tenfen.com.br>
 * @license    MIT License
 * @copyright  2018 Silvio Tenfen Junior
 */

class Orders_test extends TestCase {

	public function setUp() {
		$this->resetInstance();
		$this->CI->load->model('Orders_model');
	}
	
	public function test_get_customers() {
		$output = $this->ajaxRequest('GET', 'orders/get_customers?term=han');
		$expected = '[{"id":"5","name":"Hann Solo"}]';
		$this->assertEquals($expected, $output);
	}

	public function test_get_products()	{
		$output = $this->ajaxRequest('GET', 'products/get_products?term=mil');
		$expected = '[{"id":"1","name":"Millenium Falcon"}]';
		$this->assertEquals($expected, $output);
	}
}
