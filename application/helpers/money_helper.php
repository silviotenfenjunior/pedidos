<?php
/**
 * HELPER Money
 *
 * Functions for Manipulating Cash Values
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package general
 */

// Display decimal(10,2) format in real format
if (!function_exists('decimal2brl')) {

    function decimal2brl($decimal, $mostrar_zero = false, $mostrar_zero_para_negativos = false) {
        if ($decimal) {
            if ($decimal < 0 && $mostrar_zero_para_negativos) {
                return '0,00';
            } else {
                $decimal = number_format($decimal, 2, ',', '.');
                return $decimal;
            }
        } else {
            if ($mostrar_zero) {
                return '0,00';
            } else {
                return null;
            }
        }
    }

}

// Display real format in decimal(10,2) format
if (!function_exists('brl2decimal')) {

    function brl2decimal($decimal) {
        if ($decimal) {
            $decimal = str_replace('.', '', $decimal);
            $decimal = str_replace(',', '.', $decimal);
            return $decimal;
        } else {
            return null;
        }
    }

}
?>
