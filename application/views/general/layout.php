<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * VIEW Layout
 *
 * View da Pagina de Layout
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package backend
 */

$menu_options = array(
    'Opções' => array(
        'orders' => array('Pedidos', base_url('orders')),
    ),
);
?>
<!DOCTYPE html>
<html lang="pt-br" dir="ltr" class="uk-height-1-1">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $this->config->item('default_title') ?></title>
        <link rel="shortcut icon" href="<?php echo $this->config->item('favicon') ?>" type="image/x-icon">
        <link rel="apple-touch-icon-precomposed" href="<?php echo $this->config->item('favicon2') ?>">

        <link rel="stylesheet" href="<?php echo base_url('assets/css/uikit.gradient.min.css') ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/components/notify.min.css') ?>">

        <link rel="stylesheet" href="<?php echo base_url('assets/css/select2.min.css') ?>">

        <link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css') ?>?v=1.0">
    </head>

    <body>
        <div class="uk-container uk-container-center uk-margin-top uk-margin-large-bottom">

            <nav class="uk-navbar uk-margin-large-bottom">
                <a class="uk-navbar-brand uk-hidden-small" href="<?php echo base_url('admin_home') ?>"><img class="logo" src="<?php echo $this->config->item('system_logo') ?>" /></a>
                <ul class="uk-navbar-nav uk-hidden-small">
                    <?php
                    foreach ($menu_options as $chave => $valor) {
                        ?>
                        <li class="uk-parent" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                            <a href="#"><?php echo $chave ?></a>
                            <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-bottom" style="top: 40px; left: 0px;">
                                <ul class="uk-nav uk-nav-navbar">
                                    <?php
                                    foreach ($valor as $chave2 => $valor2) {
                                        ?>
                                        <li><a href="<?php echo $valor2[1] ?>"><?php echo $valor2[0] ?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                    <li><a href="#">Meu Usuário</a></li>
                </ul>

                <div class="uk-navbar-flip uk-hidden-small">
                    <div class="uk-navbar-content">
                        Olá, <strong>Vinícius</strong>! &nbsp; <button class="uk-button uk-button-danger uk-button-small logout" type="button">Sair</button>
                    </div>
                </div>

                <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
                <div class="uk-navbar-brand uk-navbar-center uk-visible-small"><a href="<?php echo base_url('admin_home') ?>"><img class="logo" src="<?php echo $this->config->item('logo_site') ?>" /></a></div>
            </nav>

            <?php $this->load->view($page) ?>

            <div class="uk-grid" data-uk-grid-margin="">
                <div class="uk-width-1-1 uk-text-center uk-row-first">
                    <hr/>
                    <p class="uk-text-small">© <?php echo $this->config->item('company_name').' '.date('Y') ?>. Todos os direitos reservados.</p>
                </div>
            </div>
        </div>

        <div id="offcanvas" class="uk-offcanvas">
            <div class="uk-offcanvas-bar">
                <ul class="uk-nav uk-nav-offcanvas">
                    <li><a href="#">Olá, <strong>Vinícius</strong>! &nbsp; <button onclick="javascript:window.location='<?php echo base_url('admin/sair') ?>'" class="uk-button uk-button-danger uk-button-small" type="button">Sair</button></a></li>
                    <li><a href="#">Meu Usuário</a></li>
                    <?php
                    foreach ($menu_options as $chave => $valor) {
                        echo '<li><a href="#"><strong>'.$chave.'</strong></a></li>';

                        foreach ($valor as $chave2 => $valor2) {
                            echo '<li><a href="'.$valor2[1].'">'.$valor2[0].'</a></li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </body>

    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/uikit.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/components/notify.min.js') ?>"></script>

    <script src="<?php echo base_url('assets/js/jquery.maskMoney.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/select2.full.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/i18n/select2/pt-BR.js') ?>"></script>
    
    <script>
        // Usado para definir a base_url nos arquivos JS
        var BASE_URL = '<?php echo base_url() ?>';

        <?php
        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');
        $success = isset($msg['success']) ? $msg['success'] : '';
        if ($success != ''):
            ?>
            UIkit.notify("<i class='uk-icon-bell'></i> <?php echo $success ?>", {status: 'success'});
            <?php
        endif;

        $error = isset($msg['error']) ? $msg['error'] : '';
        if ($error != ''):
            ?>
            UIkit.notify("<i class='uk-icon-bell'></i> <?php echo $error ?>", {status: 'danger'});
            <?php
        endif;

        if ( validation_errors() && !isset($not_show_validation_errors) ):
            $msg = preg_replace('/\s+/', ' ', strip_tags(validation_errors()));
            ?>
            UIkit.notify("<i class='uk-icon-bell'></i> <?php echo $msg ?>", {status: 'danger'});
            <?php
        endif;
        ?>

        <?php
        if (isset($link_to_remove) && isset($question)):
            ?>
            function deleteRecord(id) {
                window.event.preventDefault();

                UIkit.modal.labels = {
                    'Ok': 'Sim',
                    'Cancel': 'Não'
                };

                UIkit.modal.confirm("<?php echo $question ?>", function() {
                    window.location = '<?php echo $link_to_remove ?>/'+id;
                }, {center: true});
            }
            <?php
        endif;
        ?>
    </script>

    <script src="<?php echo base_url('assets/js/functions.js') ?>"></script>

    <?php echo $this->frontend->getJsCode() ?>
</html>