<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * VIEW List
 *
 * View from the Order List
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package orders
 */

?>

<div class="uk-grid">
    <div class="uk-width-3-10 titulo">
        <h2>Pedidos</h2>
        <a class="uk-button uk-button-primary" href="<?php echo base_url('orders/add') ?>">Adicionar</a>
    </div>

    <div class="uk-width-7-10 right">
        <?php echo form_open_multipart('orders','class="uk-form" id="form"') ?>
            <input type="text" name="name" placeholder="Nome" class="uk-form-width-small" value="<?php echo $data['name'] ?>" />
            <input type="hidden" name="search" value="1" />
            <button type="submit" form="form" class="uk-button">Pesquisar</button>
            <a class="uk-button" href="<?php echo base_url('orders/index/0/1') ?>">Limpar</a>
        <?php echo form_close() ?>
    </div>
</div>

<div class="uk-grid">
    <div class="uk-width-1-2">
        <ul class="uk-pagination">
            <?php echo $pages ?>
        </ul>
    </div>
    <div class="uk-width-1-2 right">
        Total de <?php echo $nrecords; ?> registros encontrados
    </div>
</div>

<div class="uk-grid">
    <div class="uk-width-1-1">
        <div class="uk-overflow-container">
            <?php
            $tmpl = array('table_open' => '<table class="uk-table uk-table-hover uk-table-striped sortable">',
                'heading_row_start' => '<tr>',
                'heading_row_end' => '</tr>',
                'heading_cell_start' => '<th>',
                'heading_cell_end' => '</th>',
                'row_start' => '<tr>',
                'row_end' => '</tr>',
                'cell_start' => '<td>',
                'cell_end' => '</td>',
                'row_alt_start' => '<tr class="dif">',
                'row_alt_end' => '</tr>',
                'cell_alt_start' => '<td>',
                'cell_alt_end' => '</td>',
                'table_close' => '</table>'
            );

            $this->table->set_template($tmpl); 

            $this->table->set_heading('ID', 'Cliente', '');

            echo $this->table->generate($records);
            ?>
        </div>
    </div>
</div>

<div class="uk-grid">
    <div class="uk-width-1-2">
        <ul class="uk-pagination">
            <?php echo $pages ?>
        </ul>
    </div>
    <div class="uk-width-1-1 right">
        Total de <?php echo $nrecords; ?> registros encontrados
    </div>
</div>