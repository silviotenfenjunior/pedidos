<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * VIEW Form
 *
 * View of the Order Form
 * @author TENFEN JUNIOR, Silvio <silvio@tenfen.com.br>
 * @version 1.0
 * @package orders
 */

$this->frontend->setJsCode('<script src="'.base_url('assets/js/orders.js').'"></script>');
?>
<div class="uk-grid">
    <div class="uk-width-1-1">
        <h2><?php echo $form_name ?></h2>
    </div>
</div>

<div class="uk-grid">
    <div class="uk-width-1-1">
        <?php
        $attributes = array('class' => 'uk-form', 'id' => 'form');
        echo form_open('orders/'.$function.'/'.$data['id'], $attributes);

        echo form_fieldset();
        ?>

        <div class="uk-form-row">
            <label for="customer_id" class="uk-form-label">Cliente</label>
            <div class="uk-form-controls">
                <select name="customer_id" id="customer_id" class="uk-width-1-1 focus get_customers">
                    <?php if ($data['customer_id'] != ''): ?>
                        <option value="<?php echo set_value('customer_id', $data['customer_id']) ?>" selected="selected"><?php echo $data['customer_name'] ?></option>
                    <?php endif; ?>
                </select>
                <?php echo form_error('customer_id'); ?>
            </div>
        </div>

        <div class="uk-form-row">
            <h3 class="subtitle">Itens do Pedido (R$)</h3>
        </div>

        <div class="uk-form-row">
            <p>Utilize o campo abaixo para pesquisar e adicionar produtos.</p>

            <div class="uk-form-controls">
                <select class="uk-width-1-1 focus get_products"></select>
                <?php echo form_error('order_items'); ?>
            </div>
        </div>

        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="uk-overflow-container">
                    <table class="uk-table uk-table-hover uk-table-striped list_order_items">
                        <thead>
                            <tr>
                                <th>Produto</th>
                                <th>Quantidade</th>
                                <th>Preço Unitário (R$)</th>
                                <th>Subtotal (R$)</th>
                                <th>Rentabilidade</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Produto</th>
                                <th>Quantidade</th>
                                <th>Preço Unitário (R$)</th>
                                <th>Subtotal (R$)</th>
                                <th>Rentabilidade</th>
                                <th></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th class="uk-text-right">Total:</th>
                                <th class="total"></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            $order_items = isset($data['order_items']) ? $data['order_items'] : array();
                            foreach ($order_items as $recs):
                                echo '<tr '.$recs['attrs'].'>';
                                
                                foreach ($recs as $field => $rec):
                                    if ($field != 'attrs'):
                                        echo '<td class="'.$field.'">'.$rec.'</td>';
                                    endif;
                                endforeach;
                                
                                echo '</tr>';
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="uk-form-row center">
            <button type="button" class="uk-button uk-button-primary validation">Salvar</button>
            <?php echo anchor('orders', 'Cancelar', 'class="uk-button"'); ?>
        </div>

        <?php
        echo form_fieldset_close();
        echo form_close();
        ?>
    </div>
</div>
