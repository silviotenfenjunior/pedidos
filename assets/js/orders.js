$(function() {
    $('.get_customers').select2({
    	language: 'pt-BR',
    	minimumInputLength: 3,
    	templateResult: formatRec,
  		templateSelection: formatRecSelection,
		ajax: {
			url: BASE_URL + 'orders/get_customers',
			dataType: 'json',
			data: function (params) {
				return {
					term: params.term
				};
			},
			processResults: function (data) {
				return {
					results: data
				};
			},
		}
	});

    $('.get_products').select2({
    	language: 'pt-BR',
    	minimumInputLength: 3,
    	templateResult: formatRec,
		ajax: {
			url: function() {
				return BASE_URL + 'products/get_products';
			},
			dataType: 'json',
			data: function (params) {
				return {
					term: params.term
				};
			},
			processResults: function (data) {
				return {
					results: data
				};
			},
		}
	});

	$('.get_products').on('select2:select', function(e) {
		$.ajax({
			url: BASE_URL + 'products/get_product/' + $(this).val(),
			dataType: 'json',
			success: function(data) {

				if ( $('.list_order_items tbody tr').hasClass('order_item_'+data.id) ) {
					$('html,body').animate({scrollTop: $('.order_item_'+data.id).offset().top}, 2000);
					$('.order_item_'+data.id).addClass('blinker');
					setTimeout(function () {
						$('.order_item_'+data.id).removeClass('blinker');
					}, 3000);

				} else {
					var qty = (data.multiple != null) ? parseInt(data.multiple) : 1;
					var unit_price = number_format(data.unit_price, 2, ',', '.');
					var subtotal = number_format(qty * parseFloat(data.unit_price), 2, ',', '.');

					var html = '<tr class="order_item_'+data.id+'" data-id="'+data.id+'" data-original_unit_price="'+data.unit_price+'" data-multiple="'+data.multiple+'">'+
						'<td class="product">'+data.name+'</td>'+
						'<td class="qty"><input type="number" name="items['+data.id+'][qty]" value="'+qty+'" class="uk-width-1-1 recalculate" data-id="'+data.id+'" /></td>'+
						'<td class="unit_price"><input type="text" name="items['+data.id+'][unit_price]" value="'+unit_price+'" class="uk-width-1-1 money recalculate" data-id="'+data.id+'" /></td>'+
						'<td class="subtotal"></td>'+
						'<td class="profitability"></td>'+
						'<td class="actions"><a href="#" onclick="deleteItem('+data.id+')" title="Remover"><i class="uk-icon-remove"></i></a></td>'+
					'</tr>';
					$('.list_order_items tbody').append(html);

					recalculate(data.id);

					$('.money').maskMoney({
						thousands: '.',
						decimal: ',',
						allowZero:true
					});
				}

				$('.get_products').val(null).trigger('change'); 
			}
		});
	});

	$(document).on('keyup mouseup', '.recalculate', function() {
		recalculate( $(this).data('id') )
	});

	$.each( $('.list_order_items tbody tr .unit_price input'), function( key, value ) {
		recalculate( $(value).data('id') )
	});

	$(document).on('click', '.validation', function() {
		validation();
	});
});

function formatRec(rec) {
  if (rec.loading) {
    return rec.text;
  }

  var markup = rec.name;

  return markup;
}

function formatRecSelection(rec) {
  return rec.name || rec.text;
}

function deleteItem(item) {
	$('.order_item_' + item).remove();
	retotalize();
}

function recalculate(product_id) {
	var qty = $('.order_item_' + product_id + ' .qty input').val();
	var unit_price = $('.order_item_' + product_id + ' .unit_price input').val().replaceAll('.', '').replaceAll(',', '.');
	var original_unit_price = parseFloat( $('.order_item_' + product_id).data('original_unit_price') );
	var subtotal = number_format( (qty * unit_price), 2, ',', '.');

	var profitability = '<span class="uk-badge uk-badge-notification uk-badge-danger">Ruim</span>';
	var profitability_data = 'bad';
	if (unit_price > original_unit_price) {
		profitability = '<span class="uk-badge uk-badge-notification uk-badge-success">Ótima</span>';
		profitability_data = 'optimum';
	
	} else if (unit_price >= ((original_unit_price / 100) * 90) ) {
		profitability = '<span class="uk-badge uk-badge-notification uk-badge-warning">Boa</span>';
		profitability_data = 'good';
	}
	
	$('.order_item_' + product_id + ' .subtotal').html(subtotal);
	$('.order_item_' + product_id + ' .profitability').html(profitability);
	$('.order_item_' + product_id).attr('data-profitability', profitability_data);

	retotalize();
}

function retotalize() {
	var total = 0;
	$.each( $('.list_order_items tbody tr'), function( key, value ) {
		var unit_price = $(value).find('.unit_price input').val().replaceAll('.', '').replaceAll(',', '.');
		var qty = $(value).find('.qty input').val();
		total = total + (qty * unit_price);
	});
	total = number_format(total, 2, ',', '.');
	$('.list_order_items .total').html('R$ ' + total);
}

function validation() {
	var ret = true;
	UIkit.modal.labels = {
        'Ok': 'OK'
    };

	if ( $('#customer_id').val() == null ) {
        UIkit.modal.alert("É obrigatório a seleção de um Cliente!", {center: true});
        ret = false;
        return false;
	}

	var number_products = 0;
	$.each( $('.list_order_items tbody tr'), function( key, value ) {
		number_products++;

		var qty = $(value).find('.qty input').val();
		if ( qty == 0 || qty == '' ) {
			UIkit.modal.alert("É obrigatório preencher todos os campos de Quantidade com valor maior que zero (0)!", {center: true});
			ret = false;
			return false;
		}

		var product = $(value).find('.product').html();
		var multiple = $(value).attr('data-multiple');
		if ( multiple > 1 && (qty % multiple) != 0 ) {
			UIkit.modal.alert("É obrigatório preencher a Quantidade de "+product+" com múltiplos de "+multiple+"!", {center: true});
			ret = false;
			return false;
		}

		var unit_price = number_format( $(value).find('.unit_price input').val(), 2, '.', '' );
		if ( unit_price == 0 || unit_price == '' ) {
			UIkit.modal.alert("É obrigatório preencher todos os campos de Preço Unitário (R$) com valor maior que zero (0)!", {center: true});
			ret = false;
			return false;
		}

		var profitability = $(value).attr('data-profitability');
		if ( profitability == 'bad' ) {
			UIkit.modal.alert("É obrigatório que todos os Itens do Pedido tenham Rentabilidade Boa ou Ótima! Por favor, ajuste os Preços Unitários!", {center: true});
			ret = false;
			return false;
		}
	});

	if ( number_products == 0 ) {
		UIkit.modal.alert("É obrigatório inserir ao menos um Produto!", {center: true});
		ret = false;
		return false;
	}

	if ( ret == true ) {
		$('#form').submit();
	}
}