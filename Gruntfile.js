module.exports = function( grunt ) {
 
  grunt.initConfig({

    rsync: {
        options: {
            args: ['-rltzuv --delete'],
            exclude: [
              ".git*",
              ".sass*",
              "node_modules",
              "cgi-bin",
              ".fuse*",
              ".directory",
              "/tmp",
              "/application/config/config.php",
              "/application/config/database.php"
            ]
        },
        upload: {
            options: {
                src: "./",
                dest: "silvio@165.227.54.53:/var/www/mercos/"
            }
        },
        download: {
            options: {
                src: "silvio@165.227.54.53:/var/www/mercos/",
                dest: "./"
            }
        },
        listonly: {
            options: {
                args: ['-rltzuv --delete --dry-run'],
                src: "./",
                dest: "silvio@165.227.54.53:/var/www/mercos/"
            }
        }
    }
 
  });
 
  // Plugins do Grunt
  // grunt.loadNpmTasks( 'grunt-contrib-uglify' );
  grunt.loadNpmTasks( 'grunt-contrib-watch' );
  grunt.loadNpmTasks( 'grunt-rsync' );
 
  // Tarefas que serão executadas
  grunt.registerTask( 'default', [ 'rsync:listonly' ] );
  grunt.registerTask( 'listonly', [ 'rsync:listonly' ] );
  grunt.registerTask( 'download', [ 'rsync:download' ] );
  grunt.registerTask( 'upload', [ 'rsync:upload' ] );
};
